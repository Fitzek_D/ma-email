﻿using System.Collections.Generic;

namespace MA_Email.Controllers
{
    public class Zeitarbeiter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gehalt { get; set; }
        public int StundenImMonat { get; set; }
        public string Email { get; set; }
        public string Handynummer { get; set; }

        public ICollection<Lohn> Loehne { get; set; }

    }
}